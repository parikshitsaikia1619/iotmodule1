# IIOT Summary

### What is IIOT?
* The **Industrial Internet of Things** (IIoT) refers to interconnected sensors, instruments, and other devices networked together with computers' industrial applications, including manufacturing and energy management. This connectivity allows for data collection, exchange, and analysis, potentially facilitating improvements in productivity and efficiency as well as other economic benefits.</br>


![IIOT logo](https://optiware.com/wp-content/uploads/2019/06/IIoT-small-image.jpg)

### Industrial Revoution: History

* **Industry 1.0** - Weave Loming,Mechanization,Steam Power
* **Industry 2.0** - Mass production began with the help of a production line
* **Industry 3.0** - This Revoution saw the evolution of electronics with automation etc.
* **Industry 4.0** - The sensors and acutators were able to send data to the cloud using certain mechanisms.

![AI logo](https://www.thedualarity.com/wp-content/uploads/Industrial-Revolution-4.0.png)

### Industry 3.0

* Industry 3.0 use Sensors, PLC's, SCADA and ERP.
* Workflow: Sensors --> PLC --> SCADA & ERP
* Communication Protocols: The Sensors installed in the factory send data to PLCs by means of Field Bus, which are basically certain Protocols such as Modbus, CANopen, EtherCAT, etc.

![pic logo](https://gitlab.com/subhad/iotmodule1/-/raw/master/assignments/summary/images/image1.png)

### Industry 4.0

Industry 4.0 is industry 3.0 devices connected to the internet (IOT) and when these devices is connected to internet they will send the data. All the protocols of industry 4.0 are optimised for sending data to cloud for data analytics.
There are various problems with industry 4.0 upgrades and some problems are mentioned so that the factory owners face and have to aware of:
* **Problem 1. Cost**: Industry 3.0 devices are very expensive as a factory owner I don’t want to switch to industry 4.0 devices because they are expensive.
* **Problem 2. Downtime**: changing hardware means a big factory downtime. I don’t want to shut down my factory to upgrade devices.
* **Problem 3. Reliability**:  I don’t want to insert in devices which are unproven and unreliable.

![industry4](https://aethon.com/wp-content/uploads/2015/07/Industry40.jpg)

### Converting Indusry 3.0 devices to Industry 4.0:
To convert a 3.0 to 4.0, a library that helps, get data from industry 3.0 devices and send to industry 4.0 cloud.
There is a roadmap for making own industrial IOT protocol by the use of industry 3.0 and industry 4.0:
* **Step1**: Identify most popular industry 3.0 devices
* **Step2**: Study protocols that these devices communicate
* **Step3**: Get data from the industry 3.0 devices
* **Step4**: Send the data to cloud for industry 4.0

### There few problems to switch to Industry 4.0 they are:

* **Cost**:Industry 4.0 devices because they are Expensive
* **Downtime**:it takes time, so no one will be willing to shutdown their factories.
* **Reliability**: There trustworthy is not proven.

### Challenges we face during Conversion

* **Expensive Hardware**: The hardware is costly.
* **Lack of documentaion**: The proper documentation is not done. Only the manufacturer knows what happens inside. And the Properitary PLC protocols.